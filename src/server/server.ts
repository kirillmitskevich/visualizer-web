// import * as Koa from 'koa';

import * as os from 'os';
import * as path from 'path';

import { Visualizer } from '../visualizer';

// const app = new Koa();

const config = {
  git: {
    remote_url: 'https://gitlab.com/kirillmitskevich/visualizer-web.git',
    local_path: path.join(os.tmpdir(), '/visualizer'),
    branch: 'master',
  },
  dataStore: {
    redis: {},
  },
};

const visualizer = new Visualizer(config);
visualizer.synchronize();

//
// app.use(async ctx => {
//   ctx.body = 'Hello World';
// });
//
// app.listen(3000);
