import * as winston from 'winston';

export const log = winston.loggers.add('visualizer-web', {
  console: {
    level: process.env.LOG_LEVEL || (process.env.NODE_ENV === 'test' ? 'warn' : 'info'),
    colorize: true,
    timestamp: true,
    prettyPrint: true,
    humanReadableUnhandledException: true,
  },
});
