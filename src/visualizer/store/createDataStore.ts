import { log } from '../../shared/log';
import { IResolvedConfig } from '../types';

import { createRedisClient } from './redis/createRedisClient';
import { RedisDataStore } from './redis/RedisDataStore';

export function createDataStore(config: IResolvedConfig) {
  if (config.has('redis')) {
    return createRedisDataStore(config.section('redis'));
  } else {
    throw new Error(
      `Can not create data store – "dataStore" section is missing in configuration file`
    );
  }
}

function createRedisDataStore(config: IResolvedConfig) {
  log.info('using Redis data store');
  const client = createRedisClient(config);
  return new RedisDataStore(client);
}
