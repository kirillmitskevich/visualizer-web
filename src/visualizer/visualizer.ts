import * as R from 'ramda';

import { log } from '../shared/log';

import { Config } from './config';
import { createDataStore } from './store/createDataStore';
import { GitDataSource } from './git/GitDataSource';
import { synchronize } from './synchronize';

import { IConfig, IConfiguration, IGitDataSource } from './types';

export class Visualizer {
  private _dataStore;
  private _git: Promise<IGitDataSource>;
  private _config: IConfig;

  constructor(args: string | IConfiguration) {
    this._config = new Config(args);

    this._git = this._config.resolve().then(config => new GitDataSource(config.section('git')));
    this._dataStore = this._config
      .resolve()
      .then(config => createDataStore(config.section('dataStore')));
  }

  async synchronize(): Promise<void> {
    try {
      await Promise.all([this._git, this._dataStore]).then(R.apply(synchronize));
    } catch (err) {
      log.error('synchronization failed: ', err);
    }
  }
}
