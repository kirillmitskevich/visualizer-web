import * as simpleGit from 'simple-git/promise';

export interface ISimpleGit extends simpleGit.SimpleGit {
  raw(command: string | string[]): Promise<any>;
  cwd(localRepoPath: string): Promise<string>;
}

export interface IRepository {
  git: ISimpleGit;
  branch: string;
}

export interface IChange {
  file: string;
  isDeleted: boolean;
}

export interface IGitDataSource {
  scanChanges(anchorCommitSha: string | null, callback: (change: IChange) => void): Promise<string>;
}
