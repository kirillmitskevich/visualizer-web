interface IGitConfiguration {
  local_path: string;
  remote_url: string;
  branch?: string;
}

export interface IConfiguration {
  git: IGitConfiguration;
}

export interface IConfig {
  resolve(): Promise<IResolvedConfig>;
}

export interface IResolvedConfig {
  optional(key: string, v: any): any;
  mandatory(key: string): any;
  section(key: string): IResolvedConfig;
  has(key: string): boolean;
}
