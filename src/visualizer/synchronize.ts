import { log } from '../shared/log';
import { IChange, IGitDataSource } from './types';

export async function synchronize(dataSource: IGitDataSource, dataStore): Promise<void> {
  log.info('synchronization begin...');

  const headAnchor: string = await dataSource.scanChanges(null, (change: IChange) => {
    log.debug(`synchronization change: ${JSON.stringify(change)}`);
  });

  log.info(`synchronization complete;`, `head is: [${headAnchor}]`);
}
