class CoreError extends Error {
  constructor(message: string, errorName: string) {
    super(message);

    this.name = errorName;
    this.message = message;

    if (typeof Error.captureStackTrace === 'function') {
      Error.captureStackTrace(this, this.constructor);
    } else {
      this.stack = new Error(message).stack;
    }
  }
}

export class ConfigError extends CoreError {
  constructor(message: string, sectionName?: string) {
    const prefix = sectionName ? `configuration error: ${sectionName}: ` : 'configuration error: ';
    super(prefix + message, 'ConfigError');
  }
}

export class GitError extends CoreError {
  constructor(message: string) {
    super(message, 'GitError');
  }
}
