import { IResolvedConfig, IConfig, IConfiguration } from './types';
import { ConfigError } from './errors';

export class Config implements IConfig {
  readonly _promise;

  constructor(source: string | IConfiguration) {
    if (typeof source === 'string') {
      console.log('Config: source is string. Handle loading config attributes using provided path');
    } else if (typeof source === 'object') {
      this._promise = Promise.resolve(resolveConfig(source));
    } else {
      this._promise = Promise.reject(
        new ConfigError(`invalid configuration parameter: ${typeof source}`)
      );
    }
  }

  resolve(): Promise<IResolvedConfig> {
    return this._promise;
  }
}

function resolveConfig(attributes: IConfiguration): IResolvedConfig {
  return new ResolvedConfig(attributes);
}

export class ResolvedConfig implements IResolvedConfig {
  readonly _attributes: Partial<IConfiguration>;
  readonly _sectionName: string;

  constructor(attributes: Partial<IConfiguration> = {}, sectionName: string = '') {
    this._attributes = attributes;
    this._sectionName = sectionName;
  }

  optional(key: string, defaultValue: any): any {
    return this._attributes[key] || defaultValue;
  }

  section(key: string): IResolvedConfig {
    const newSectionAttributes = this._attributes[key];
    const newSectionName = this._sectionName ? `${this._sectionName}.${key}` : key;

    // throw new ConfigError(`option [${key}] should be an object`, this._sectionName);

    return new ResolvedConfig(newSectionAttributes, newSectionName);
  }

  has(key: string): boolean {
    return Boolean(this._attributes[key]);
  }

  mandatory(key: string): any {
    if (!this._attributes[key]) {
      throw new ConfigError(`missing mandatory option [${key}]`, this._sectionName);
    }

    return this._attributes[key];
  }
}
