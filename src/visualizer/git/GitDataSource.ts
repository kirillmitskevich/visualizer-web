import * as simpleGit from 'simple-git/promise';
import * as fs from 'fs';

import { IChange, IGitDataSource, IRepository, IResolvedConfig, ISimpleGit } from '../types';
import { log } from '../../shared/log';
import { GitError } from '../errors';

export class GitDataSource implements IGitDataSource {
  private _branch;
  private _remoteUrl;
  private _localPath;

  constructor(config: IResolvedConfig) {
    this._branch = config.optional('branch', 'master');
    this._localPath = config.mandatory('local_path');
    this._remoteUrl = config.mandatory('remote_url');
  }

  async scanChanges(anchorCommitSha, callback: (change: IChange) => void) {
    const repository: IRepository = await loadRepository(
      this._localPath,
      this._remoteUrl,
      this._branch
    );

    //  if anchor
    //    scan changes since commit
    //  else
    //    scan changes since tail

    await scanChangesSinceTail(repository, callback);

    return await getLastCommitSha(repository);
  }
}

async function getLastCommitSha(repository: IRepository) {
  const { git, branch } = repository;
  const sha = await git.revparse([branch]);

  return sha.trim();
}

async function scanChangesSinceTail(repository: IRepository, callback: (change: IChange) => void) {
  log.verbose('git: scanning changes since tail...');

  const { git, branch } = repository;
  const command = ['ls-tree', '-r', '--name-only', branch];

  const diff: string = await git.raw(command);

  diff
    .trim()
    .split('\n')
    .forEach(filePath => {
      callback({
        file: filePath,
        isDeleted: false,
      });
    });
}

async function loadRepository(
  localPath: string,
  remoteUrl: string,
  branch: string
): Promise<IRepository> {
  if (folderExists(localPath)) {
    return await reuseExistingRepository(localPath, remoteUrl, branch);
  }

  return await cloneRepository(localPath, remoteUrl, branch);
}

async function reuseExistingRepository(
  localPath: string,
  remoteUrl: string,
  branch: string
): Promise<IRepository> {
  log.verbose(`git: reuse existing repository at ${localPath}`);

  const git: ISimpleGit = simpleGit(localPath) as ISimpleGit;

  try {
    log.verbose(`git: pulling changes from ${remoteUrl} (branch: ${branch})...`);
    await git.pull(remoteUrl, branch);
    log.verbose(`git: pulling changes complete`);
  } catch (err) {
    throw new GitError(`failed to pull changes into branch [${branch}] from ${remoteUrl}: ${err}`);
  }

  return { git, branch };
}

async function cloneRepository(
  localPath: string,
  remoteUrl: string,
  branch: string
): Promise<IRepository> {
  log.verbose(`git: cloning repository from ${remoteUrl} to ${localPath}...`);

  const git: ISimpleGit = simpleGit() as ISimpleGit;

  try {
    await git.clone(remoteUrl, localPath);
    log.verbose(`git: cloning repository complete`);
    await git.cwd(localPath);
  } catch (err) {
    throw new GitError(`failed to clone repository from ${remoteUrl}: ${err}`);
  }

  return { git, branch };
}

function folderExists(localPath: string): boolean {
  if (fs.existsSync(localPath)) {
    const stats = fs.lstatSync(localPath);
    return stats.isDirectory();
  }

  return false;
}
